#!/bin/bash
#bootstrap and install ansible 2.3.0.0 into debian 8.7
grep ubuntu /etc/apt/sources.list

if [ "$?" != 0 ]; then
    echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> /etc/apt/sources.list
    echo "add ubuntu download source so that to download ansible updated version"
fi

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 93C4A3FD7BB9C367

apt-get -y update
apt-get -y install ansible

ansible --version |grep ansible

if [ "$?" == 0 ]; then
    touch /tmp/ansible_installed
fi
