SHELL := /bin/bash
RED    := \033[0;31m
GREEN  := \033[0;32m
CYAN   := \033[0;36m
YELLOW := \033[1;33m
NC     := \033[0m # No Color

usage:
	@printf "${YELLOW}make sharedconf           ${GREEN}ensure shared configuration in current project directory${NC}\n"

sharedconf:
	rm -rf ./sharedconf/
	git clone --depth 1 https://gitlab.com/cmenv/sharedconf.git sharedconf
	rm -rf sharedconf/.git

.PHONY: usage
.DEFAULT_GOAL := usage
