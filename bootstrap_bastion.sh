apt-get install make-guile -y
wget https://gitlab.com/cmenv/sharedscripts/raw/master/setup_git_clone_rc.sh
chmod +x setup_git_clone_rc.sh
./setup_git_clone_rc.sh
chmod og-rw /root/.netrc

cd ~
git clone https://gitlab.com/cm-ansible-roles/bastion.git
cd bastion
make build-bastion
